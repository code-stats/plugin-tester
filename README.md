# plugin-tester

Utility to help in Code::Stats editor plugin development.

## Requirements

* Python 3.7+. Tested with Python 3.8.

## Running

* `python run.py -p 1234` would run on port 1234.
* Set your editor plugin's URL to point to http://localhost:1234/api/my/pulses or whatever host/port
  you are running the tool on.
* Observe the calls being printed in the console.
