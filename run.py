#!/usr/bin/env python3

import argparse
import http.server
import json
import urllib.request
import ssl
from datetime import datetime, timezone

VERSION = "1.1.0"
SSL_CONTEXT = ssl.create_default_context()

api_token = None
total_xps = {}
forwards = []


class PluginTesterRequestHandler(http.server.BaseHTTPRequestHandler):
    def do_POST(self):
        global api_token, total_xps, forwards

        if self.path in ("/api/my/pulses", "/api/my/pulses/"):
            if "x-api-token" not in self.headers or self.headers["x-api-token"] != api_token:
                self.log_error("Invalid or missing API token")
                self.json(403, {"error": "You must be authenticated"})
                return

            data = self.rfile.read(int(self.headers.get("content-length")))
            try:
                body = json.loads(data)
            except Exception as e:
                self.log_error(f"Cannot parse JSON payload: {e}")
                self.json(400, {"error": "Cannot parse JSON payload"})
                return

            if "coded_at" not in body or "xps" not in body:
                self.log_error("Malformed JSON, xps or coded_at missing")
                self.json(400, {"error": "Malformed JSON"})
                return

            # Python's parser doesn't understand Z so convert it
            coded_at = body["coded_at"].upper().replace("Z", "+00:00")

            # Python's parser doesn't understand offset without :
            if coded_at[-3] != ":":
                coded_at = coded_at[0:-2] + ":" + coded_at[-2:]

            try:
                coded_at_dt = datetime.fromisoformat(coded_at)
            except ValueError as e:
                self.log_error(f"Cannot parse coded_at: {e}")
                self.json(400, {"error": "Malformed timestamp"})
                return

            now = datetime.now(timezone.utc)
            delta = now - coded_at_dt

            if delta.total_seconds() > 7 * 24 * 60 * 60:
                self.log_error("coded_at too far in the past (> 1 week)")
                self.json(400, {"error": "Invalid date."})
                return

            xps = {}

            if type(body["xps"]) != list:
                self.log_error("xps not a list")
                self.json(400, {"error": "Invalid XP format."})
                return

            for xp in body["xps"]:
                if "language" not in xp or "xp" not in xp or type(xp["language"]) != str or type(xp["xp"]) != int:
                    self.log_error(f"XP {json.dumps(xp)} is invalid")
                    self.json(400, {"error": "Invalid XP format."})
                    return

                xps[xp["language"]] = xps.get(xp["language"], 0) + xp["xp"]

            self.log_message("Got XP successfully:")
            for (language, xp) in xps.items():
                self.log_message(f"{language}: {xp}")
                total_xps[language] = total_xps.get(language, 0) + xp

            self.log_message("Current total amounts:")
            for (language, xp) in total_xps.items():
                self.log_message(f"{language}: {xp}")

            self.json(201, {"ok": "Great success!"})

            if len(forwards) > 1:
                self.forward(data)
        else:
            self.log_error(f"Unknown path '{self.path}'")
            self.json(404, {})

    def json(self, status, msg):
        self.send_response(status)
        self.send_header("content-type", "application/json; charset=utf-8")
        self.end_headers()
        self.wfile.flush()
        self.wfile.write(json.dumps(msg).encode("utf-8"))

    def forward(self, data):
        for (url, key) in forwards:
            req = urllib.request.Request(url, data, method="POST")
            req.add_header("content-type", "application/json")
            req.add_header("x-api-token", key)
            req.add_header("user-agent", f"code-stats-plugin-tester/{VERSION}")
            print(f"Sending to {url}... ", end="")

            try:
                resp = urllib.request.urlopen(req, context=SSL_CONTEXT)
                if resp.getcode() == 201:
                    print("OK")
            except Exception as e:
                print("ERROR")
                print(e)


cli_parser = argparse.ArgumentParser(
    description="Code::Stats plugin tester tool")
cli_parser.add_argument("api_token", type=str, help="API token to expect")
cli_parser.add_argument("-H", "--host", type=str, default="localhost",
                        help="Host to bind to (default: localhost)")
cli_parser.add_argument("-p", "--port", type=int,
                        default=8080, help="Port to listen on (default: 8080)")
cli_parser.add_argument("-f", "--forward", type=str, action="append",
                        help="Forward pulses to given API. Specify as pair of '<api-token>@<api-url>'. Give flag multiple times for multiple forwards. (default: no forwarding)", default=[])

cli_args = cli_parser.parse_args()
api_token = cli_args.api_token
for f in cli_args.forward:
    parts = f.split("@")
    key = parts[0]
    url = "@".join(parts[1:])
    forwards.append((url, key))

server = http.server.ThreadingHTTPServer(
    (cli_args.host, cli_args.port), PluginTesterRequestHandler)
print(f"Listening on {cli_args.host}:{cli_args.port}...")

if len(forwards) > 0:
    print(f"Forwarding to API(s): {', '.join([i[0] for i in forwards])}")

server.serve_forever()
